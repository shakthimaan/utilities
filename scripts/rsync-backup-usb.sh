#!/bin/bash

DISK="$1" # /dev/sdb1
DEVICE="usb-sandisk"
MOUNT="/mnt"

if [ -z "$1" ]; then
  echo "Error: Pass disk partition as an argument!"
  exit 1
fi

# Mount USB sandicks
echo "#"
echo "# Mounting USB disk"
echo "#"
sudo cryptsetup luksOpen "$DISK" "$DEVICE"
sudo cryptsetup luksDump "$DISK"

sudo mount "/dev/mapper/$DEVICE" "$MOUNT"

# Remove ~ files
echo "#"
echo "# Remove ~ files"
echo "#"
find ~/backup/daily -name *~ -exec rm -f {} \;
find ~/.ssh -name *~ -exec rm -f {} \;

# Constants
USER=/home/shakthi

# daily/
echo "#"
echo "# rsync"
echo "#"
sudo rsync -avzh ~/backup/daily /mnt/backup

# HOME
FILES="$USER/.emacs $USER/.emacs.d $USER/.bashrc $USER/.ssh $USER/.stumpwmrc $USER/.Xdefaults $USER/.config/chromium/Default/Bookmarks"

echo "#"
echo "# rsync "
echo "#"
for file in $FILES
do
  sudo rsync -avzh $file /mnt/backup/HOME/
done

# Close
echo "#"
echo "# Close"
echo "#"
sudo umount "$MOUNT"
sudo cryptsetup luksClose "$DEVICE"
