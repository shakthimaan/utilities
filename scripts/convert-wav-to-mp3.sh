#!/bin/sh

# sh convert-wav-to-mp3.sh folder

mkdir -p $1/mp3

find $1 -name "* *" -execdir rename " " "-" "{}" \;

for file in $1/*; do
  name=`basename "${file%.*}"`
  target="$1/mp3/${name}.mp3"
  echo "Converting ${file} with ${name} to ${target}"
  ffmpeg -i "${file}" -codec:a libmp3lame -qscale:a 2 "${target}"
done
