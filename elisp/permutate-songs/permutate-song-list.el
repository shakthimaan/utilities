;;; permutate-song-list --- Summary

;;; Commentary:
;;; பாடல்கள் வரிசைமாற்றி இயக்கும் கணினி நிரல்

;;; Code:

(require 'dash)

(defconst songs-per-day 4)
(defconst total-songs 26)

;; வரிசைமாற்றம்
;; Donald Knuth's shuffle algorithm
(defun shuffle (seq)
  "SEQ is the input list."
  (let ((n (length seq)))
    (dotimes (i n seq)
      (rotatef (elt seq i)(elt seq (+ i (random (- n i))))))))

;; The main function
(defun main ()
  "SHUFFLE is the permutation function.
SONG-LIST_NEW is the input song list."
  (setq result (-partition-all songs-per-day
                           (shuffle (number-sequence 1 total-songs))))


  (mapcar (lambda (l) (sort l '<)) result))

(main)

(provide 'permutate-song-list)

;;; permutate-song-list ends here
